module Memoization
  @@return_value = Hash.new
  def memoize(method_name)
    priv_method = "_#{method_name}_"
    alias_method priv_method, method_name
    define_method method_name do |arg|
      if(@@return_value[arg] == nil)
        return @@return_value[arg] = send(priv_method, arg)
      else
        return @@return_value[arg]
      end
    end
  end
end