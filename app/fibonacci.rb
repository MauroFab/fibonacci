require_relative 'memoization.rb'
class Fibonacci 
  extend Memoization
  def at_position(pos)
    return pos if pos < 2
    at_position(pos - 1) + at_position(pos - 2)
  end
  memoize :at_position
end